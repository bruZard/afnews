function DetailView() {
	var self = Ti.UI.createView({
		backgroundColor:'fff'
	});
    
    var scroll = Ti.UI.createScrollView({
        contentWidth: '100%',
        contentHeight: 'auto',
        width: '95%',
        showVerticalScrollIndicator: true,
        showHorizontalScrollIndicator: false
    });
    	
	var webView = Ti.UI.createWebView({
		font: {fontSize: 16, fontFamily: 'Roboto'},
		scalesPageToFit: false,
		ebableZoomControls: false
	});
	scroll.add(webView);
	self.add(scroll);

	self.addEventListener('itemSelected', function(e) {
		webView.html = e.descr;
	});

	return self;
};

module.exports = DetailView;
