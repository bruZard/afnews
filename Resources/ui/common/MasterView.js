//Master View Component Constructor
function MasterView() {
	//create object instance, parasitic subclass of Observable
	var self = Ti.UI.createView({
		backgroundColor:'white'
	});
	
	// eingestellte Sprache auslesen und entsprechendes PHP File einsezen
	var lang = Ti.Locale.currentLanguage;
	var php = "rss.php";
	if(lang != "de") php = "rss_en.php";
	
	// deutsches Datum - Tagesnamen
	var tage = new Array();
	tage["Mon"] = "Mo.";
	tage["Tue"] = "Di.";
	tage["Wed"] = "Mi.";
	tage["Thu"] = "Do.";
	tage["Fri"] = "Fr.";
	tage["Sat"] = "Sa.";
	tage["Sun"] = "So.";
	
	var tableData = [];
    var url = "http://www.amigafuture.de/" + php;
    var xhr = Titanium.Network.createHTTPClient();
    var rowHeight = 'auto';
    
    if(Ti.Platform.osname == 'android') rowHeight = '60dp';
    
    xhr.onload = function(e){
        var doc = Ti.XML.parseString(this.responseText.replace(/iso-8859-1/i, 'UTF-8')).documentElement;
        var items = doc.getElementsByTagName("item");
        if(items != null && items.length > 0){
            try{
            	var s = 0;
            	var _background = '#aaaaaa';
            	
                for(var i = 0; i < items.length; i++){
                    // .replace(/<[^>]+>/g, '')
                    var _titleElement = items.item(i).getElementsByTagName("title").item(0);
                    var _descrElement = items.item(i).getElementsByTagName("description").item(0);
                    var _linkElement = items.item(i).getElementsByTagName("link").item(0);
                    var _pubElement = items.item(i).getElementsByTagName("pubDate").item(0);
                    
                    var _title = _titleElement.text;
                    var _descr = _descrElement.text;
					var _link = _linkElement.text;
					var _pub = _pubElement.text;
					
					var _dateParts = _pub.split(' ');
					var clock = _dateParts[4].split(':');
					
					var datum = _dateParts[0] + ' ' + _dateParts[1] + ' ' + _dateParts[2] + ' ' + _dateParts[3] + ' ' + clock[0] + ':' + clock[1];
										
					if(lang === "de"){
						datum = tage[_dateParts[0].replace(',', '')] + ' den ' + _dateParts[1] + ' ' + _dateParts[2] + ' ' + _dateParts[3] + ' um ' + clock[0] + ':' + clock[1] + ' Uhr';
					}					
					
					Ti.API.info(datum);
					
					if(s == 1){
						_background = '#dddddd';
					}else{
						_background = '#ffffff';
					}
					
					var lblDatum = Ti.UI.createLabel({
						text: datum,
						font: {fontSize: 12},
						width: '100%'
					});
					
					var lblTitle = Ti.UI.createLabel({
						text: _title,
						font: {fontSize: 22}
					});
					
                	var row = Ti.UI.createTableViewRow({
                    	/*title: _title,*/
                    	descr: _descr,
                    	color: '#000',
                    	height: rowHeight,
                    	font: {fontSize: 22, fontFamily: 'Roboto'},
                    	backgroundColor: _background
                    });
                    
                    row.add(lblDatum);
                    row.add(lblTitle);
                    
                    tableData.push(row);
                    
                    s = 1 - s;
                    //tableData[tableData.length] = {title: _title, descr: _descr};
                }
                table.data = tableData;
            }catch(e){
                console.log(e);
            }
        }
    };
    
    xhr.setRequestHeader("Content-Type", "application/xml; charset=iso-8859-1");
    xhr.open('POST', url);
    xhr.send();
    
	var table = Ti.UI.createTableView({
		data:tableData,
		opacity: 1.0,
		seperatorColor: '#999',
		font: {fontSize: 22, fontFamily: 'Roboto'}
	});
	self.add(table);

	//add behavior
	table.addEventListener('click', function(e) {
		self.fireEvent('itemSelected', {
			title:e.rowData.title,
			descr:e.rowData.descr
		});
	});

	return self;
};

module.exports = MasterView;